# CHANGE LOG
## 1.6.4 (2018-08-17)
### Features

### BugFixes
1. [elastalert发送的payload不符合契约无法发送成功](://gitlab.niudingfeng.com/OpsDev/AMC/issues/23)
2. [告警内容当有非法字符时，微信无法发送一个提示告警](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/24)


## 1.6.2 (2018-07-27)
### Features
1. [运维各角色的具体联系人从代码中迁移到配置文件](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/21)

## 1.6.1 (2018-07-20)
### Features
1. [TTS出现业务流控时，应通过微信和邮件通知对方](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/20)

## 1.6.0 (2018-07-13)
### Features
1. 更新为python3
2. 增加禁止实际发送开关，用于测试

### BugFixes

## 1.5.7 (2018-07-06)
### Features
1. [发送告警日志增加中文名信息](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/18)
2. [当微信发送告警给运维总监时，应保证对应告警邮件也发给运维总监](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/19)

### BugFixes

## 1.5.6 (2018-06-29)
### Features

### BugFixes
1. [告警恢复的通知人有误](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/16)
2. reopen issue [tag_id同步，只同步被请求过的](https://gitlab.niudingfeng.com/OpsDev/EmployeeInfo/issues/5)

## 1.5.5 (2018-6-22)
### Features
1. [邮件标题引用message字段](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/15)

### BugFixes
1. [AMC的日志格式不一致](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/14)

## 1.5.4
### Features
1. [ElastAlert的微信接收人为空时，运维总监不接收微信告警](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/12)
2. [首页新增代码的issues提交链接和CHANGELOG链接](https://gitlab.niudingfeng.com/OpsDev/AMC/issues/13)
3. 查询employee info服务，区分不同环境

### BugFixes
1. 更改CMDB IP为CMDB域名


## 1.5.3
### Features
1. debug接口新增git hash返回，用于判断解决docker相同tag却由不同代码版本的问题

## 1.5.2
### Features
1. 微信tag_id查询，从直接查询改为从employee_info服务中获取

## 1.5.1
### BugFixes
1. 修复tag_id的数据类型错误
2. 增加log日志体积，提高被采集率

## 1.5.0
### Features:
1. 增加Kapacitor Post告警的发送
2. 更新ElastAlert告警器

### Bug Fix：
1. 无法同步员工信息时，复用旧有信息，当旧有信息也为空时，停止过滤离职员工发送功能
2. 修复ElastAlert发送员工的默认值问题
3. 修复ElastAlert未给email或user_id赋值时的AMC错误
4. 修复微信发送失败重发的未定义变量问题

### Maintenance:
1. 采用pipenv替换requirements.txt
2. 更新swagger文件为V1.3.0，增加ElastAlert和Kapacitor的API定义

## 1.4.0
### Features
1. 新增微信tag_id对应的用户信息获取，实现解析tag_id为user_ids，并取消原有的tag_id发送。（收益：避免user_id和tag_id的重复让员工重复接收告警；拆分后，可以准确得实现按用户的告警统计）。程序与微信服务器的tag用户信息1小时同步一次。
2. user_ids和emails加入非在职人员的过滤
3. ElastAlert传递过来的user_ids和emails通过非在职过滤后，如果两者全部为空，则从CMDB查询运维人员信息
4. 本地内存查询员工在职和员工联系方式，提高可靠性并加快告警发送速度
5. 运维总监的告警信息实现定制化，加入接收告警的员工名单，提高管理便捷性

