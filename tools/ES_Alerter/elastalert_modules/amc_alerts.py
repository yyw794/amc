# coding:utf-8
import sys
import requests
from elastalert.alerts import Alerter, BasicMatchString

reload(sys)
sys.setdefaultencoding('utf8')

class AmcAlerter(Alerter):
    #required_options = set(['amc_url', 'cmdb_ip', 'log_level', 'log_path'])
    def __init__(self, *args, **kwargs):
        super(AmcAlerter, self).__init__(*args, **kwargs)
        if self.rule.get('amc_url') is None:
            if self.rule.get('index') and '.pre' in self.rule.get('index'):
                self.rule['amc_url'] = "https://ops-amc-pre.niudingfeng.com"
            else:
                self.rule['amc_url'] = "https://ops-amc.niudingfeng.com"

    def alert(self, matches):
        for match in matches:
            match_string = str(BasicMatchString(self.rule, match))
            payload = {
                'match': match_string,
                'user_id': self.rule.get('user_id', ''),
                'tag_id': str(self.rule.get('tag_id', '')),
                'email': self.rule.get('email', [])
            }
            ret = requests.post(self.rule['amc_url'] + '/api/v1/alert/elastAlert', json=payload)

    def get_info(self):
        return  {
            'type': 'AmcAlerter', 
            'user_id': self.rule.get('user_id', ''),
            'tag_id': self.rule.get('tag_id', ''),
            'email': self.rule.get('email', [])
            }



