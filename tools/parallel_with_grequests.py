import grequests

URL = "http://traefik-testing.niudingfeng.com/amc/api/v1/debug"
URL = "http://localhost:5000/api/v1/debug"
TIMES = 1000


def get_ip_parallel():
    rs = (grequests.get(URL) for i in range(TIMES))
    rets = grequests.map(rs)
    datas = [ret.json() for ret in rets if ret and ret.status_code == 200]
    print(datas[0])


def post_send_message(payload):
    url = "http://traefik-testing.niudingfeng.com/amc" + "/api/v1/alert"

    rs = (grequests.post(url, json=payload) for i in range(TIMES)) 
    rets = grequests.map(rs) 
    datas = [ret.json() for ret in rets if ret and ret.status_code == 201]
    print(len(datas))
    print(datas[0])    

payload_wechat = {
    'send_type': 'wechat',
    'to': ['xn080520'],
    'content': 'test'
}
payload_email = {
    'send_type': 'email',
    'to': ['yanyongwen@xiaoniu66.com'],
    'content': 'test'
}
payload = payload_email
for i in range(1):
	post_send_message(payload)
