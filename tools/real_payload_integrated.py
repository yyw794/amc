from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import  String, Column, Integer, DateTime, JSON
import requests
import sys

engine = create_engine("mysql+mysqldb://yan_rw:e7F90B94B87F153_@10.17.8.41:33000/yan_db?charset=utf8")
Session = sessionmaker(bind=engine)

Base = declarative_base()

AMC_URL = 'http://localhost:5000'

class AmcRecPayload(Base):
    __tablename__ = "amc_rec_payload"  
    id = Column(Integer, primary_key=True)
    rec_time = Column(DateTime)
    alert_type = Column(String(50))
    payload = Column(JSON)
    env = Column(String(20))

    def __init__(self, rec_time=None, alert_type='', payload={}, env=''):
        self.rec_time = rec_time
        self.alert_type = alert_type
        self.payload = payload
        self.env = env

    def __repr__(self):
        return '<Payload %r>' % (self.payload)

session = Session()

def test_elast_alert():
    fail_range = []
    # elastAlert count() mainly 3000
    for i in range(3000):
        print(f"for i {i}")
        try:
            elast_alert_records = session.query(AmcRecPayload).filter(AmcRecPayload.env == 'production').filter(AmcRecPayload.alert_type == 'elastAlert').limit(1).offset(i).all()
        except Exception as e:
            fail_range.append(i)
            print(e)
            #session.query(AmcRecPayload).filter(AmcRecPayload.rec_time == elast_alert_records[0].rec_time).delete()
            #elast_alert_records.delete()
            continue

        for each in elast_alert_records:
            print(each.rec_time, each.alert_type, each.env)
            response = requests.post(f'{AMC_URL}/api/v1/alert/elastAlert', json=each.payload)
            if response.status_code < 400:
                if 'xn000003' in each.payload["user_id"]:
                    print(f"delete {each.rec_time} {each.alert_type} {each.env}")
                    session.query(AmcRecPayload).filter(AmcRecPayload.rec_time == each.rec_time).delete()
            else:
                print(response.status_code)
                print(response.json())
                print(each.payload)

    print(fail_range)
    session.commit()

def test_standard_alert():
    fail_range = []

    error_range = []

    for i in range(10000):
        print(f"for i {i}")
        try:
            alert_records = session.query(AmcRecPayload).filter(AmcRecPayload.env == 'production').filter(AmcRecPayload.alert_type == 'Alert').limit(1).offset(i).all()
        except Exception as e:
            fail_range.append(i)
            print(e)
            continue

        for each in alert_records:
            #print(each.rec_time, each.alert_type, each.env)
            response = requests.post(f'{AMC_URL}/api/v1/alert', json=each.payload)
            if response.status_code >= 400:
                error_range.append(response.rec_time)
                print(response.status_code)
                print(response.json())
                print(each.payload)

    print(fail_range)
    print(error_range)
    session.commit()

#test_elast_alert()
test_standard_alert()