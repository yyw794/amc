from bravado.client import SwaggerClient
import json
import urllib2
import inspect
import sys
from pprint import pprint

#CHANGEME
test_url = '10.17.2.178:5000'
swagger_file_location='swaggerhub'
FILE_LOCATIONS = ('swaggerhub', 'test', 'prod')
server = 'test'
SERVER_LOCATIONS = ('test', 'prod')

def get_swagger_json(swagger_file_location='swaggerhub'):
	'''
	return json object of swagger.json
	'''
	# if you has no need to change json, use SwaggerClient.from_url() will easier
	assert swagger_file_location in FILE_LOCATIONS

	if swagger_file_location == 'swaggerhub':
		# CHANGEME
		url = 'https://api.swaggerhub.com/apis/niudingfeng/niudingfeng-amc_api/1.0.0'
	else:
		_url = test_url if swagger_file_location == 'test' else "ops-amc.niudingfeng.com"
		url = 'http://{}/v1/swagger.json'.format(_url)

	req = urllib2.Request(url)

	if swagger_file_location == 'swaggerhub':
		api_key = '''eyJUb2tlblR5cGUiOiJBUEkiLCJzYWx0IjoiYWJiZjYwZDEtNzQ0ZC00ZDdjLThjOGYtMjVlYjQ0YmMyODc3\
IiwiYWxnIjoiSFM1MTIifQ.eyJqdGkiOiI1YTRhYmRkYy00OWZjLTQ2MTktYjE5NC04ODMxZjQwMDAzZTEiLCJpYXQiOjE1MTczN\
zQ4MzF9.qrD-6cHGmd3jTmNDrHpGP4_U04t3qeuJWUGz3fMPWczFo0jcrwv7QEp6OSiCW1lWildqgOeNI-nZc16P3STbRg'''
		req.add_header('Authorization', api_key)

	return json.load(urllib2.urlopen(req))	

def get_swagger_client(server='test'):
	assert server in SERVER_LOCATIONS

	swagger_json = get_swagger_json('swaggerhub')

	if server == 'test':
		swagger_json['host'] = test_url
	#pprint(swagger_json)

	client = SwaggerClient.from_spec(swagger_json)
	return client

def get_tts(client):
	ret = client.tts.findByKeyword(keyword='18620300840').result()
	#print(ret)
	ret = client.tts.findByBizid(bizid='113957366462^100769061462').result()
	print(ret)
	print(ret['data']['listened'])
	return ret['data']['listened']

def post_send_alert(client):
	# SendAlert is from definitions in swagger
	AlertBody = client.get_model('alertBody')
	#print(AlertBody.__init__.__code__.co_varnames)

	alert_body = AlertBody(send_type="email", to="yanyongwen@xiaoniu66.com", content="bravado swagger")

	ret = client.alert.sendAlert(body=alert_body).result()
	print(ret)
	return ret

if __name__ == '__main__':
	client = get_swagger_client('test')
	print(client)
	get_tts(client)
	post_send_alert(client)
