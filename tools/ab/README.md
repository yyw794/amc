###安装
```bash
sudo yum install httpd-tools
```

###使用
#### 测试http网页
```bash
ab -n 100 -c 10 http://${your_url}
```
-n代表一共发送100次
-c代表一次发送10次（并发量为10）

#### 测试rest接口
假设rest接口采用post json的方式
将json数据写在一个文件上，如：post_email.txt
```json
{"send_type":"email", "to":"yanyongwen@xiaoniu66.com", "content":"parallel test"}
```
```bash
ab -n 100 -c 10 -p post_wechat.txt -T application/json http://${your_url}
```
-p代表你要发送的数据文件
-T那说明数据是json

##### 修改header
```bash
ab -n 1000 -c 200 -p post.txt -H 'X-Custom-Header':'xxxx' -H 'OS-TYPE':'linux' -T application/json http://localhost:9090/hardware
```
通过-H的方式来在header里添加内容，支持多个-H书写。

### 结果分析
重点查看：
Requests per second，代表QPS，每秒可以处理的数量能力；
