import configparser
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch.connection import create_ssl_context
from datetime import datetime
from dateutil.parser import parse as parse_date

#ROOT_PATH = os.path.split(os.path.realpath(__file__))[0]

#config = configparser.RawConfigParser()
#config.read('config.cfg')
def print_search_stats(results):
    print('=' * 80)
    print('Total %d found in %dms' % (results['hits']['total'], results['took']))
    print('-' * 80)

def print_hits(results):
    " Simple utility function to print results of a search query. "
    print_search_stats(results)
    for hit in results['hits']['hits']:
        print('/%s/%s/%s' % (hit['_index'], hit['_type'], hit['_id']))
        print(hit['_source']['message'])

    print('=' * 80)
    print()

ES_USER = 'ops' #config.get("ES", "user")
ES_PASSWORD = 'qwerty' #config.get("ES", "password")
ES_HOST = '10.17.2.42' #config.get("ES", "url")
ES_PORT = 9200 #config.get("ES", "port")
ES_INDEX = 'ndf.amc-2018.08' #config.get("ES", "index")

#pem_path = os.path.join(ROOT_PATH, '..', 'cert/niudingfeng.pem')
#ssl_context = create_ssl_context(cafile=pem_path)

es = Elasticsearch(hosts=[{'host': ES_HOST, 'port': ES_PORT}],
                        #ssl_context=ssl_context,
                        #scheme="https",
                        http_auth=(ES_USER, ES_PASSWORD)
                        )


#interest_keys = ['message']

doc = {
    'author': 'kimchy',
    'text': 'Elasticsearch: cool. bonsai cool.',
    'timestamp': datetime.now(),
}
#res = es.index(index=ES_INDEX, doc_type='tweet', id=1, body=doc)
#print(res['result'])
#data = es.search(index=ES_INDEX, q='wechat', _source_include=['message'], size=1)
body = {
        "query": {
                "bool": {
                        "must": {
                                "match": {"frontend_name": "amc", "status_code": 500}
                        }
                }
        }
}
#data = es.search(index=ES_INDEX, doc_type='doc', body=body)
data = es.search(index=ES_INDEX, doc_type='doc', body=body)
print_hits(data)

'''
client = es
print(client)
s = Search(using=client, index=ES_INDEX) \
	.filter("term", category="search") \
	.query("match", title="告警")
    #.exclude("match", description="beta")

response = s.execute()



for hit in s:
	print(hit.title)

print(s.to_dict())
'''