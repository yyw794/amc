import os 
import urllib2

def download_swagger_file():
	'''
	refenece: https://app.swaggerhub.com/help/apis/downloading-swagger-definition
	'''
	#CHANGEME
	owner = 'niudingfeng'
	api = 'niudingfeng-amc_api'
	version = '1.3.0'
	api_key = '''eyJUb2tlblR5cGUiOiJBUEkiLCJzYWx0IjoiYWJiZjYwZDEtNzQ0ZC00ZDdjLThjOGYtMjVlYjQ0YmMyODc3\
IiwiYWxnIjoiSFM1MTIifQ.eyJqdGkiOiI1YTRhYmRkYy00OWZjLTQ2MTktYjE5NC04ODMxZjQwMDAzZTEiLCJpYXQiOjE1MTczN\
zQ4MzF9.qrD-6cHGmd3jTmNDrHpGP4_U04t3qeuJWUGz3fMPWczFo0jcrwv7QEp6OSiCW1lWildqgOeNI-nZc16P3STbRg'''
	
	#download
	url = 'https://api.swaggerhub.com/apis/{}/{}/{}'.format(owner, api, version)
	req = urllib2.Request(url)
	req.add_header('Authorization', api_key)
	download_json = urllib2.urlopen(req)

	req.add_header('Accept', 'application/yaml')
	download_yaml = urllib2.urlopen(req)

	dir_path = os.path.dirname(os.path.realpath(__file__))
	print(dir_path)

	#save
	with open(os.path.join(dir_path, 'swagger.json'), 'w') as swagger_file:
		swagger_file.write(download_json.read())

	with open(os.path.join(dir_path, 'swagger.yaml'), 'w') as swagger_file:
		swagger_file.write(download_yaml.read())

if __name__ == '__main__':
	download_swagger_file()