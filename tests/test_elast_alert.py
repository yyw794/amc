#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import json
import os
import subprocess
import sys
import time
import textwrap

import pytest
from flask.wrappers import Response

from flask import request

sys.path.append(".")
from src.amc import app, send_alert
from src.elast_alert import send_elast_alert
import src.elast_alert as elast_alert

PERSONS_CONTACT = app.config.get("PERSONS_CONTACT")


class MockResponse:
	def __init__(self, json_data={}, status_code=200):
		self.json_data = json_data
		self.status_code = status_code

	def json(self):
		return self.json_data

@pytest.fixture
def client():
	return app.test_client()

HOSTNAME = 'twin-sz01-zx-prd-db-001.16qian.cn'

@pytest.fixture
def payload():
	match_string = textwrap.dedent(
		'''
		 告警名称:AMC测试用例告警
		 告警类型:匹配error关键词
		 累计命中数:5
		 新增告警数:2
		 告警时间:2018-05-04 10:39 CST
		 日志类型:var
		 告警主机: {}
		 日志路径:/var/log/messages
		 项目名称:dzq
		 项目模块:application
		 日志内容:这是AMC的测试用例告警产生，不用惊慌
		'''.format(HOSTNAME)
		)

	payload = {
		'match': match_string,
		# dengminghua, ouyangbin
		'user_id': "xn035452|xn037722|xn026618",
		'email': ['yanyongwen@xiaoniu66.com'],
		# tag_id 36 contains xn080520
		'tag_id': "36|37"
	}
	return payload

@pytest.fixture
def payload_only_matchstring(payload):
	payload.pop('user_id')
	payload.pop('email')
	payload.pop('tag_id')
	return payload

def test_integration_elastalert_payload_2_amc_payload(client, payload):
	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)

	assert len(payloads) == 4

	assert payloads[0]['to'] == sorted(['yanyongwen@xiaoniu66.com'])
	# 'xn035452' from user_id
	# 'xn060264', 'xn080520' from tag_id
	# 'xn037722', 'xn026618' both from user_id and tag_id
	assert sorted(payloads[1]['to']) == sorted(['xn037722', 'xn060264', 'xn080520'])
	assert 'AMC测试用例告警' in payloads[0]['subject']

	# for ops director
	assert sorted(payloads[2]['to']) == sorted([PERSONS_CONTACT.get('ops_director')['email']])
	assert sorted(payloads[3]['to']) == sorted([PERSONS_CONTACT.get('ops_director')['employee_id']])
	assert '告警接收人员' in payloads[2]['content'] and '严勇文' in payloads[2]['content']
	assert '告警接收人员' in payloads[3]['content'] and '严勇文' in payloads[3]['content']
	assert payloads[2]['subject'] == 'ElastAlert: AMC测试用例告警'

def test_elastalert_payload_lackof_alert_name_and_alert_host(client, payload):
	problem_match_string = textwrap.dedent(
		'''
 		服务名称:user-service-register
 		日志类型:类型:cn.xn.user.service.impl.RegisterServiceImpl
 		累计命中数:3
 		新增告警数:1
 		告警时间:2018-08-14 11:13 CST
 		AgentID:8c9b16bbb8c5
 		TraceID:                 115342164032440CNMtotisterbbb8c545821f6d956c465c918f6bac7cfb87b7
 		告警内容:~~~~[13719909181]查询手机号已被注册,请求结束~~~ \n\n"
		''')

	payload['match'] = problem_match_string
	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)
	assert payloads[0].get('content').replace('\t', '').replace('\n', '').replace(' ', '') \
	== problem_match_string.replace('\t', '').replace('\n', '').replace(' ', '') 


def test_integration_elastalert_payload_2_amc_payload_judged_by_CMDB(client, payload_only_matchstring):
	# sz-rjds-4f-dzq-prd-paper-001.16qian.cn owner and ops are dengminghui only
	# it will get data from CMDB and EmployeeInfo with restful API
	payload = payload_only_matchstring
	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)
	assert [PERSONS_CONTACT.get('dba')['email']] == payloads[0]['to']
	assert [PERSONS_CONTACT.get('dba')['employee_id']] == payloads[1]['to']

def test_integration_elastalert_payload_2_amc_payload_all_invalid_then_judged_by_CMDB(client, payload_only_matchstring):
	# sz-rjds-4f-dzq-prd-paper-001.16qian.cn owner and ops are dengminghui only
	# it will get data from CMDB and EmployeeInfo with restful API
	payload = payload_only_matchstring
	# 1 is CEO; 2 is CEO brother..., 3,4 is not existed
	payload['user_id'] = "xn000003|xn000004"
	payload['email'] = ["xxxx@xiaoniu66.com"]

	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)

	assert [PERSONS_CONTACT.get('dba')['email']] == payloads[0]['to']
	assert [PERSONS_CONTACT.get('dba')['employee_id']] == payloads[1]['to']

	# test tag_id not exist situation
	payload['tag_id']  = "0"
	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)

	assert [PERSONS_CONTACT.get('dba')['email']] == payloads[0]['to']
	assert [PERSONS_CONTACT.get('dba')['employee_id']] == payloads[1]['to']


def test_integration_hostname_not_exist_elastalert_payload_2_amc_payload(client, payload_only_matchstring):
	payload = payload_only_matchstring
	FAKE_HOSTNAME = 'xxx-fake-hostname'
	payload['match'] = payload['match'].replace(HOSTNAME, FAKE_HOSTNAME)

	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)

	to = payloads[0]['to']
	content = payloads[0]['content']
	# if hostname is invalid , send to cmdb admin
	assert PERSONS_CONTACT.get("cmdb_admin")['email'] in to
	for each in PERSONS_CONTACT.get("system_ops"):
		assert each['email'] in to
	assert PERSONS_CONTACT.get("ops_director_assistant")['email'] in to
	assert "主机名 {} 不存在".format(FAKE_HOSTNAME) in content

def mock_cmdb_get_hostname_info(mocker, mock_data):
	mocker.patch.object(send_elast_alert.cmdb_api, 'get_node_info_by_hostname')
	mock_resp = MockResponse(mock_data, 200)
	send_elast_alert.cmdb_api.get_node_info_by_hostname.return_value = mock_resp

def test_mock_cmdb_lack_of_ops_elastalert_payload_2_amc_payload(client, payload_only_matchstring, mocker):
	mock_data = {'alarm_contact': 
					{
					   'ops': [],
					   'owner': {'name': '刘亮', 'employee_id': 'xn014179',
						'email': 'liuliang@xiaoniu66.com', 'phone': '18028794650'}
					}
				}
	mock_cmdb_get_hostname_info(mocker, mock_data)

	payload = payload_only_matchstring
	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)

	to = payloads[0]['to']
	content = payloads[0]['content']
	subject = payloads[0].get('subject')

	assert 'AMC测试用例告警' in subject
	assert '错误提示' not in content
	# get owner if ops is empty
	assert PERSONS_CONTACT.get('ops_director_assistant')['email'] in to


def test_mock_cmdb_lack_of_ops_and_owner_elastalert_payload_2_amc_payload(client, payload_only_matchstring, mocker):
	mock_data = {'alarm_contact': 
					{
					   'ops': [],
					   'owner': {'name': '', 'employee_id': '','email': '', 'phone': ''}
					}
				}
	mock_cmdb_get_hostname_info(mocker, mock_data)

	payload = payload_only_matchstring
	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)

	to = payloads[0]['to']
	content = payloads[0]['content']

	assert "错误提示" in content
	assert "运维人员和负责人都没有填写" in content
	# if contacts are empty, you should add liuliang
	assert PERSONS_CONTACT.get('ops_director_assistant')['email'] in to

def test_integration_elastalert_payload_2_amc_payload_no_wechat(client, payload_only_matchstring):
	# sz-rjds-4f-dzq-prd-paper-001.16qian.cn owner and ops are dengminghui only
	# it will get data from CMDB and EmployeeInfo with restful API
	payload = payload_only_matchstring
	#payload['user_id'] = None
	payload['email'] = ["yanyongwen@xiaoniu66.com"]

	payloads, _, _ = send_elast_alert.elastalert_payload_2_amc_payload(payload)

	assert len(payloads) == 1

	for each in payloads:
		assert each["send_type"] == 'email'


@pytest.fixture
def kapacitor_payload():
	details = textwrap.dedent(
		'''
		告警名称: Test-AMC测试KapacitorAlert
		告警类型: 磁盘使用率: Warn >=88 Crit >= 93
		告警级别: WARNING
		告警时间:2018-05-04 10:39 UTC
		告警主机: {}
		磁盘路径: /data/ceph/osd.18.254
		磁盘使用率: 91%
		'''.format(HOSTNAME)
		)

	payload = {
		'details': details,
		'level': 'WARNING',
		'previousLevel': 'OK',
		'message': 'AMC测试KapacitorAlert'
	}
	return payload

@pytest.fixture
def kapacitor_headers():
	headers={'warn_email': 'yanyongwen@xiaoniu66.com', 'warn_wechat': 'xn000003', 
		'crit_email': 'yanyongwen@xiaoniu66.com|yanyongwen1@xiaoniu66.com', 'crit_wechat': 'xn000003|xn000004'}
	return headers

def test_send_kapacitor_alert(client, kapacitor_payload, kapacitor_headers, mocker):
	#mock_send_elastalert_alert(mocker)
	mocker.spy(elast_alert, 'send_elastalert_alert')
	response = client.post('/api/v1/alert/kapacitorAlert', data=json.dumps(kapacitor_payload), 
		headers=kapacitor_headers ,content_type='application/json')
	assert response.status_code == 201
	es_payload = {
        'match': kapacitor_payload.get('details'),
        'email': ['yanyongwen@xiaoniu66.com'],
        'user_id': 'xn000003',
        'subject': kapacitor_payload.get('message')
    }
	elast_alert.send_elastalert_alert.assert_called_once_with(es_payload)

def test_send_kapacitor_alert_severity(client, kapacitor_payload, kapacitor_headers, mocker):
	#mock_send_elastalert_alert(mocker)
	mocker.spy(elast_alert, 'send_elastalert_alert')

	kapacitor_payload['level'] = 'OK'
	kapacitor_payload['previousLevel'] = 'CRITICAL'


	response = client.post('/api/v1/alert/kapacitorAlert', data=json.dumps(kapacitor_payload), 
		headers=kapacitor_headers ,content_type='application/json')
	assert response.status_code == 201
	es_payload = {
        'match': kapacitor_payload.get('details'),
        #TODO: why is this order?
        'email': ['yanyongwen1@xiaoniu66.com', 'yanyongwen@xiaoniu66.com'],
        'user_id': 'xn000003|xn000004',
        'subject': kapacitor_payload.get('message')
    }
	elast_alert.send_elastalert_alert.assert_called_once_with(es_payload)
