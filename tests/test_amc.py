#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import json
import os
import subprocess
import sys
import time

import pytest
from flask.wrappers import Response
from top.api.base import TopException

sys.path.append(".")
from src.amc import SendTts, app, SendWechat, SendEmail

@pytest.fixture
def client():
	return app.test_client()

def test_readme(client):
	response = client.get('/')
	assert "AMC" in response.get_data(True)
	assert response.status_code == 200

def test_swagger_files(client):
	for surfix in (".yaml", ".json"):
		file_name = 'swagger{}'.format(surfix)
		response = client.get('/api/v1/' + file_name)
		assert response.status_code == 200
		with open(file_name, 'w') as swagger_file:
			swagger_file.write(response.get_data(True))
		ret = subprocess.call(["swagger-cli", "validate", file_name])
		ret_rm = subprocess.call(["rm", file_name])
		assert ret == 0
		assert ret_rm == 0

@pytest.mark.skip
def test_get_tts_result(client):
	response = client.get('/api/v1/alert/tts/findByBizid?biz_id=' + '113957366462^100769061462')
	assert response.status_code == 200
	json_data = json.loads(response.data)
	assert json_data["data"]["listened"] == False

@pytest.mark.skip
def test_get_tts_result_wrong_bizid(client):
	for each in ('111111111^222222222', '113957366462^100769061463'):
		response = client.get('/api/v1/alert/tts/findByBizid?biz_id=' + each)
		assert response.status_code == 400
		json_data = json.loads(response.data)
		assert 'ValueError: There is no such biz_id in ES' == json_data["error"]["message"]

@pytest.mark.skip
def test_get_tts_keyword(client):
	phone = '18620300840'
	response = client.get('/api/v1/alert/tts/findByKeyword?keyword=' + phone)
	assert response.status_code == 200
	json_data = json.loads(response.data)
	for each in json_data["data"]["items"]:
		assert each["callee"] == phone

def test_post_wechat_content_too_long(client):
	payload = {
		'send_type': 'wechat',
		'to': ['xn080520'],
		'content': 200*'祝你好运!'
	}
	response = client.post('/api/v1/alert', data=json.dumps(payload), content_type='application/json')
	assert response.status_code == 201

 
def test_post_wechat_content_invalid(client):
	content = '''微信unicode escape 测试[u'\\u76d1\\u63a7\\u5e73\\u53f0']'''
	payload = {
		'send_type': 'wechat',
		'to': ['xn080520'],
		'content': content
	}
	response = client.post('/api/v1/alert', data=json.dumps(payload), content_type='application/json')
	assert response.status_code == 201

def assert_post_response_code(client, data, status_code=201):
	response = client.post("/api/v1/alert", data=json.dumps(data), content_type='application/json')
	assert response.status_code == status_code
	return response

def __post_data_correct(client, data, expected_data={'data':{}, 'error':{'code':0, 'message':''}}, status_code=201):
	response = assert_post_response_code(client, data, status_code)

	json_data = json.loads(response.data)
	assert json_data == expected_data	

def test_post_email(client, mocker):
	mocker.patch.object(client, 'post')
	resp = Response(status=201)
	resp.set_data('''
		{
		"data": {},
		"error": {"code": 0, "message": ""}
		}
		''')
	client.post.return_value = resp

	data = {
		"send_type": "email",
		"to": ["yanyongwen@xiaoniu66.com"],
		"content": "amc restful api test"
	}
	__post_data_correct(client, data)

def test_post_wechat_default(client, mocker):
	mocker.patch.object(client, 'post')
	resp = Response(status=201)
	resp.set_data('''
		{
		"data": {},
		"error": {"code": 0, "message": ""}
		}
		''')
	client.post.return_value = resp
	data = {
		"send_type": "wechat",
		"to": ['xn080520'],
		"content": "amc restful api test"
	}
	__post_data_correct(client, data)

def test_post_wechat_docker(client, mocker):
	mocker.patch.object(client, 'post')
	resp = Response(status=201)
	resp.set_data('''
		{
		"data": {},
		"error": {"code": 0, "message": ""}
		}
		''')
	client.post.return_value = resp

	data = {
		"send_type": "wechat",
		"to": ['xn080520'],
		"content": "amc restful api test",
		"channel": "docker"
	}
	__post_data_correct(client, data)

def test_post_wechat_zabbix(client, mocker):
	mocker.patch.object(client, 'post')
	resp = Response(status=201)
	resp.set_data('''
		{
		"data": {},
		"error": {"code": 0, "message": ""}
		}
		''')
	client.post.return_value = resp

	data = {
		"send_type": "wechat",
		# my account has no permission
		"to": ['xn025949'],
		"content": "amc restful api test",
		"channel": "zabbix"
	}
	__post_data_correct(client, data)


def test_post_wechat_no_permission(client):
	# this is a real test
	# my account has no permission
	data = {
		"send_type": "wechat",
		"to": ['xn080520'],
		"content": "amc restful api test",
		"channel": "zabbix"
	}
	expected_data = {
		'data': {},
		'error': {
			'code': -1 ,
			'message': 	"ValueError: ('wechat', ['xn080520'], WeChatClientException(82001, All touser & toparty & totag invalid))"
		}
	}
	__post_data_correct(client, data, expected_data, status_code=400)
	

def test_post_wechat_multi_receivers(client, mocker):
	mocker.patch.object(client, 'post')
	resp = Response(status=201)
	resp.set_data('''
		{
		"data": {},
		"error": {"code": 0, "message": ""}
		}
		''')
	client.post.return_value = resp
	data = {
		"send_type": "wechat",
		"to": ['xn080520', 'xn025949'],
		"content": "amc restful api test",
		"channel": "docker"
	}
	__post_data_correct(client, data)

def test_tts_filter_forbidden_char():
	assert "" == SendTts._filter_forbidden_char(SendTts.FORBIDDEN_TOKEN)

	for not_allowed in (100, ""):
		assert SendTts.ILLEGAL_INFO == SendTts._filter_forbidden_char(not_allowed)


def test_post_tts(client):
	data = {
		"send_type": "tts",
		# fake number
		"to": ['25162504'],
		"content": "amc restful api test",
	}
	# even you send a fake number, ali will response 201. 
	#201 only means ali get your request, send successful or not, 201 does not promise
	assert_post_response_code(client, data, status_code=201)

def test_tts_mock(client, mocker):
	mocker.patch.object(SendTts, 'send_tts')
	biz_ids = ['794']
	SendTts.send_tts.return_value = biz_ids
	
	data = {
			"send_type": "tts",
			"to": ['18620300840'],
			"content": "test mock"
	}
	expected_data = {
		'data': {'items': biz_ids},
		'error': {
			'code': 0 ,
			'message': ''
		}
		 }
	__post_data_correct(client, data, expected_data)

	assert SendTts.send_tts.call_count == 1
	

def test_tts_resend_all_fail(client, mocker):
	mocker.patch.object(SendTts, 'send_tts')
	error = TopException()
	error.message = 'subcode=isv.BUSINESS_LIMIT_CONTROL'
	SendTts.send_tts.side_effect = error
	mocker.patch.object(time, 'sleep')

	#mocker.patch.object(SendTts, 'send_alert_with_wechat_email_if_tts_fail')

	mocker.patch.object(SendWechat, 'send_message')
	
	data = {
			"send_type": "tts",
			"to": ['18620300840'],
			"content": "test mock"
	}
	expected_data = {
		'data': {},
		'error': {
			'code': -1 ,
			'message': 'Tts Resend Failed !!'
		}
	}
	__post_data_correct(client, data, expected_data, 408)

	assert SendTts.send_tts.call_count == 2
	assert time.sleep.call_count == 0
	#assert SendTts.send_alert_with_wechat_email_if_tts_fail.call_count == 1
	assert SendWechat.send_message.call_count == 1

def test_tts_resend_then_success(client, mocker):
	mocker.patch.object(SendTts, 'send_tts')
	biz_ids = ['794']
	SendTts.send_tts.return_value = biz_ids
	error = TopException()
	error.message = 'subcode=isv.BUSINESS_LIMIT_CONTROL'
	SendTts.send_tts.side_effect = [error, mocker.DEFAULT]
	mocker.patch.object(SendTts, 'switch_channel')
	mocker.patch.object(time, 'sleep')
	
	data = {
			"send_type": "tts",
			"to": ['18620300840'],
			"content": "test mock"
	}
	expected_data = {
		'data': {'items': biz_ids},
		'error': {
			'code': 0 ,
			'message': ''
		}
		 }
	__post_data_correct(client, data, expected_data)
	assert SendTts.send_tts.call_count == 2
	assert SendTts.switch_channel.call_count == 1
	assert time.sleep.call_count == 0
def test_tts_forbidden_send(client, mocker):
	mocker.patch.object(SendTts, 'send_tts')
	biz_ids = ['794']
	SendTts.send_tts.return_value = biz_ids
	error = TopException()
	error.message = 'xxx'
	SendTts.send_tts.side_effect = [error, mocker.DEFAULT]
	
	data = {
			"send_type": "tts",
			"to": ['18620300840'],
			"content": "test mock"
	}
	expected_data = {
		'data': {'items': biz_ids},
		'error': {
			'code': 0 ,
			'message': ""
		}
	}
	__post_data_correct(client, data, expected_data)
	assert SendTts.send_tts.call_count == 2
	#SendTts.send_tts.assert_called_with('18620300840', "test mock")
	SendTts.send_tts.assert_called_with(['18620300840'], "有一条告警语音发送不成功，请从你的邮件和微信查阅该告警信息")


def test_get_debug(client):
	response = client.get('/api/v1/debug')

	assert 200 == response.status_code

	json_data = json.loads(response.data)
	version = json_data.get('version', '')
	major, minor, patch = version.split(".")
	assert int(major) or int(minor) or int(patch)

