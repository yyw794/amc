ARG IMAGE=nexus.niudingfeng.com:8083/opsdev/python:3.7-alpine-fast
FROM $IMAGE
RUN apk add --update --no-cache libffi-dev python-dev mariadb-dev mariadb-client git && rm -rf /var/cache/apk/*
WORKDIR /usr/src/app
COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock
RUN pipenv install --system --deploy
RUN mkdir -p /data/logs && chmod a+w /data/logs
ENV THREAD_NUM 5
ENV LOG_APP_NAME AMC
COPY . .
CMD gunicorn --threads ${THREAD_NUM} -b 0.0.0.0:5000 --log-file=/data/logs/amc.log --logger-class src.amc.GunicornLogger --access-logfile=/data/logs/amc_access.log --access-logformat='%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"' -e FLASK_ENV=${FLASK_ENV} -e LOG_APP_NAME=${LOG_APP_NAME} src.amc:app
