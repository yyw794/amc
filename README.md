# Welcome to AMC!!
(version: 1.7.0)

## How to Use(for Users)

### Recommend Restful API:
[Restful API](api/v1/ui)

you can download API files:

[Swagger.yaml](api/v1/swagger.yaml)

[Swagger.json](api/v1/swagger.json)

### Issues
[Issues](https://gitlab.niudingfeng.com/OpsDev/AMC/issues)

### CHANGE LOG
[CHANGE LOG](https://gitlab.niudingfeng.com/OpsDev/AMC/blob/develop/CHANGELOG.md)



## How to Deploy(for Ops)

### use DOCKER to deploy(recommend)

we use gunicorn to be web server in production

if you need parallel handle, pass PROCESS_NUM env

PROCESS_NUM means workers, 4-12 is enough, recommend (2x$num_cores + 1)

```bash
docker run -p 5000:5000 -e PROCESS_NUM=4 -d --name=amc amc
```



