from src.amc import app

if __name__ == '__main__':
    # DO NOT USE THIS DEFAULT WEB SERVER IN PRODUCTION
    # if debug is True, it will not write to log file
    app.run(host='0.0.0.0', debug=True, port=app.config.get('PORT'))