## OLD API(below v1)

### How to send alert

you can post one json object or list of json objects to $AMC_URL/send

each json object MUST contains: `send_type, to, content`

send_type only support: `tts, email, wechat `
(tts means text-to-speech)

wechat has sub-type, we call it channel:
channel ONLY support: `alert, docker, zabbix`
alert: 牛鼎丰平台告警中心
docker：小牛容器云平台
zabbix： 系统监控

#### Example:
##### send a tts alert
the phone number can be string or number type, 18620300840 and "18620300840" are both right

```bash
curl -d '{"send_type":"tts", "to":18620300840, "content":"磁盘空间100%"}' $AMC_URL/send
```

##### send an email alert
the email address can be string or list type, list type means you can send multi persons together.
"yanyongwen@xiaoniu66.com", ["yanyongwen@xiaoniu66.com"], ["hujiaxin@xiaoniu66.com", "ouyangbin@xiaoniu66.com"] are all right.

```bash
curl -d '{"send_type":"email", "to":["yanyongwen@xiaoniu66.com"], "content":"CPU负载98%"}' $AMC_URL/send
```

##### send a wechat alert
wechat has many channels, by default is alert channel if you did not specify the channel

```bash
curl -d '{"send_type":"wechat", "to":"xn080520", "content":"这是一个测试告警"}' $AMC_URL/send
```

you can specify the channel like this

```bash
curl -d '{"send_type":"wechat", "to":"xn080520", "content":"这是一个测试告警", "channel":"alert"}' $AMC_URL/send
```

### How to get Response/Feedback/Result
After you send your data, you will get a response which is json type, like
```json
{
	"err_code": 0,
	"err_msg": ""
}
```
If your send is success, the err_code must be 0.

ATTENTION!!
``Email``
The SMTP of xiaoniu66 has bug, even the receiver email is invalid, it will return success.

#### How to get TTS real result asynchronously
##### Backgroud you should know
If you send Tts and get err_code is 0, it only means Ali Service Platform handle your request.
BUT, Ali Service Platform still need to deliver your speech to Mobile Operator, this process may be fail or rejected.
You need to get the real result from Ali Service later with another SDK.
Luckily, we build a service to poll real result from Ali Service and save it in ElasticSearch.
We offer you a convenient way to get real result.

##### Use GET api
if you send tts, you will get response like:
```json
{
  "biz_id": [
    "113948027318^100759742318"
  ], 
  "err_code": 0, 
  "err_msg": ""
}
```
`biz_id is list, which means you can send multi phones at the same time`

several minutes later, you can try to get real result with our get api.
url: $AMC_URL/get
send_type: ("tts")
func: ("biz_id"|"keyword")
content: 

###### How to judge the alert tts has been listened or not
if func is "biz_id", we will judge the whether the user has listened over 5 seconds of the call.
if so, listened status is true, or else is false.

```bash
curl -d '{"send_type":"tts", "func":"biz_id", "content":"113948027318^100759742318"}' $AMC_URL/get
```
the return response like:
```json
{
  "listened": false
}
```

if you get :
```json
{
  "err_code": -1, 
  "err_msg": "ValueError: There is no such biz_id in ES"
}
```
Maybe you need to wait a moment and then try, or else your biz_id is wrong

###### How to search the info in ES
As the result is stored in ES, you can search by keyword like this:
```bash
curl -d '{"send_type":"tts", "func":"keyword", "content":"18620300840"}' $AMC_URL/get
```