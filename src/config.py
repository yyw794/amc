import os

env = os.getenv("FLASK_ENV", default="development")


# pylint: disable=too-few-public-methods
class Config:
    EMAIL = {
        "smtp_server": "mail.xiaoniu66.com",
        "user": "ndf.monitor",
        "password": "Qaz1wsx#xn",
        "email": "ndf.monitor@xiaoniu66.com",
    }

    WECHAT = {
        "docker_id":
        "wxa9dbf4b3a54457cd",
        "docker_secret":
        "1fv8oJT3PxNQ5p-q8N_Kn8Z_qlIZ2gD99iR9TpPiwbf6Gzk6EuwlcUQyt3P5UTjD",
        "docker_agent_id":
        "67",
        "zabbix_id":
        "wxa9dbf4b3a54457cd",
        "zabbix_secret":
        "d4mVdPEg4ICJZhyS2g43R-W4YFvbIf6kETqCRFhmgyuSMGLLtnvqPQw9LfO2OBF0",
        "zabbix_agent_id":
        "34",
        "alert_id":
        "wxa9dbf4b3a54457cd",
        "alert_secret":
        "1X5NbONFHIKFPR-5rgsq_EpO37RcvyldbYU9L0qJgIR8eyjpx3vj9auXkEHb_XPc",
        "alert_agent_id":
        "58",
    }

    TTS = {
        "url": "eco.taobao.com",
        "port": 443,
        "https": True,
        "appkey": "24661931",
        "secret": "ad7a4a37a42bce29431c9f91ce3ee0a6",
        "caller": "075536567300",
        "caller_bak": "075536567320",
        "tts_code": "TTS_69960004",
    }

    LOG = {"level": "debug", "path": "/data/logs/amc.log"}

    ES = {
        "port": 9205,
        "url": "ela.niudingfeng.com",
        "user": "amc",
        "password": "123456qwerty",
        "index": "ndf.alert.amc",
    }

    CMDB = {
        "user": "cmdb_api",
        "password": "Xhh2FG2lwmoChJ0t",
        "ip": "http://jumpserver-bak.niudingfeng.com",
    }

    ELASTIC_APM = {
        # allowed characters in SERVICE_NAME: a-z, A-Z, 0-9, -, _, and space
        "SERVICE_NAME": "AMC_{}".format(env.upper()),
        "SERVER_URL": "http://10.17.2.42:8200",
        # IMPORTANT: THIS IS NECESSARY!!
        "DEBUG": True,
    }

    PORT = 5000
    EMPLOYEE_INFO_USER = "devops"
    EMPLOYEE_INFO_PASSWORD = "go_DevOps"
    # product and staging env can visit test env...
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "mysql://yan_rw:e7F90B94B87F153_@10.17.8.41:33000/yan_db"

    PERSONS_CONTACT = {
        "ops_director": {
            "name": "伍礼明",
            "employee_id": "xn026618",
            "email": "wuliming1@xiaoniu66.com",
            "phone": "18926195445",
        },
        "author": {
            "name": "严勇文",
            "employee_id": "xn080520",
            "email": "yanyongwen@xiaoniu66.com",
            "phone": "18620300840",
        },
        "cmdb_admin": {
            "name": "欧阳彬",
            "employee_id": "xn037722",
            "email": "ouyangbin@xiaoniu66.com",
            "phone": "18566754186",
        },
        "system_ops": [
            {
                "name": "吕开能",
                "employee_id": "xn060264",
                "email": "lvkaineng@xiaoniu66.com",
                "phone": "18898760220",
            },
            {
                "name": "谢国波",
                "employee_id": "xn035243",
                "email": "xieguobo@xiaoniu66.com",
                "phone": "13728631946",
            },
        ],
        "ops_director_assistant": {
            "name": "刘亮",
            "employee_id": "xn014179",
            "email": "liuliang@xiaoniu66.com",
            "phone": "18028794650",
        },
        "dba": {
            "name": "童罗",
            "employee_id": "xn058946",
            "email": "tongluo@xiaoniu66.com",
            "phone": "18575522218",
        },
    }


class DevelopmentConfig(Config):
    EMPLOYEE_INFO_URL = "http://traefik-testing.niudingfeng.com/employee_info"
    # EMPLOYEE_INFO_URL = 'http://localhost:7943'


class StagingConfig(Config):
    EMPLOYEE_INFO_URL = "https://pre-api-ops.niudingfeng.com/employee_info"
    # EMPLOYEE_INFO_URL = 'https://employee-info-pre.niudingfeng.com'


class ProductionConfig(Config):
    ELASTIC_APM = {
        # allowed characters in SERVICE_NAME: a-z, A-Z, 0-9, -, _, and space
        "SERVICE_NAME": "AMC",  # 'AMC_{}'.format(env.upper()),
        # 'SECRET_TOKEN': 'heLrQaQKyf6eiUjpIcdyEpsOB6XT6u',
        "SERVER_URL": "http://logs.niudingfeng.com:8200",
        "DEBUG": True,
    }
    EMPLOYEE_INFO_URL = "https://employee-info.niudingfeng.com"


env_lower = env.lower()
if (env_lower.startswith("develop") or env_lower.startswith("feature")
        or env_lower.startswith("test")):
    config_object = DevelopmentConfig
elif env_lower == "staging":
    config_object = StagingConfig
else:
    config_object = ProductionConfig

print(config_object.__name__)
