import json
import textwrap

import requests
from flask import request

from apscheduler.schedulers.background import BackgroundScheduler
from pytz import utc

from .amc import app, send_alert
from ndf_devops_utils.api.employee_info_api import EmployeeInfoApi

import traceback


class SendElastAlert:
    def __init__(self):
        self.persons_contact = app.config.get("PERSONS_CONTACT")

        self.cmdb_api = CMDB_API(
            app.config.get("CMDB").get("ip"),
            app.config.get("CMDB").get("user"),
            app.config.get("CMDB").get("password"),
        )

        self.employee_info_api = EmployeeInfoApi(
            app.config.get("EMPLOYEE_INFO_URL"),
            app.config.get("EMPLOYEE_INFO_USER"),
            app.config.get("EMPLOYEE_INFO_PASSWORD"),
            app.logger,
        )
        self.employee_info_api.get_all_users()
        self.employee_info_api.update_tag_ids()

        self.update_employee_info_periodic()

    def update_employee_info_periodic(self):
        scheduler = BackgroundScheduler()
        scheduler.configure(timezone=utc)
        scheduler.add_job(
            self.employee_info_api.get_all_users, "interval", hours=24)
        scheduler.add_job(
            self.employee_info_api.update_tag_ids, "interval", hours=1)
        scheduler.start()

    @staticmethod
    def get_value_from_match_string(match_string, key):
        lines = match_string.split("\n")
        for line in lines:
            if key in line:
                value = line.split(":")[1]
                return value.strip()

        app.logger.warning(f"{key} is empty")
        return ""

    @staticmethod
    def get_ops_contacts(hostname_info):
        contacts = hostname_info["alarm_contact"]
        ops_contacts = contacts["ops"]
        if not ops_contacts:
            owner = contacts["owner"]
            # if it is empty,
            if owner.get("name"):
                app.logger.debug("add owner %s", owner)
                ops_contacts.append(owner)
        return ops_contacts

    def parse_elastalert_match_str(self, match_string):
        hostname = self.get_value_from_match_string(match_string, "告警主机")
        alert_name = self.get_value_from_match_string(match_string, "告警名称")
        return alert_name, hostname

    def parse_user_ids_and_emails_from_elastalert(self, payload):
        user_ids = ([x for x in payload.get("user_id").split("|")
                     if x != ""] if payload.get("user_id") else [])
        tag_ids = (
            [x for x in str(payload.get("tag_id")).split("|")
             if x != ""] if payload.get("tag_id") else [])
        emails = payload.get("email", [])
        for tag_id in tag_ids:
            app.logger.info(tag_id)
            tag_id_users = self.employee_info_api.get_tag_id_users(tag_id)
            app.logger.info(tag_id_users)
            user_ids += tag_id_users
        # filter the valid users
        emails = [
            x for x in emails
            if self.employee_info_api.is_employee_email_valid(x)
        ]
        user_ids = [
            x for x in user_ids
            if self.employee_info_api.is_employee_uid_valid(x)
        ]

        return user_ids, emails

    def get_ops_receivers(self, hostname):
        ops_contacts = []
        my_error = ""
        try:
            ret = self.cmdb_api.get_node_info_by_hostname(hostname)
            if ret.status_code == 200:
                ops_contacts = self.get_ops_contacts(ret.json())
                if not ops_contacts:
                    my_error = "错误提示：主机 {} 运维人员和负责人都没有填写".format(hostname)
                    ops_contacts.append(
                        self.persons_contact.get("ops_director_assistant"))
                    raise NameError(my_error)
            else:
                my_error = "错误提示：主机名 {} 不存在".format(hostname)
                ops_contacts.append(self.persons_contact.get("cmdb_admin"))
                ops_contacts.append(
                    self.persons_contact.get("ops_director_assistant"))
                ops_contacts.extend(self.persons_contact.get("system_ops"))
                raise NameError(my_error)
        except NameError as e:
            app.logger.debug(traceback.format_exc())
            app.logger.warning(e)

        return ops_contacts, my_error

    def get_receivers_from_CMDB_if_no_receivers_given(self, user_ids, emails,
                                                      hostname):
        my_error = ""
        if user_ids == [] and emails == []:
            ops_contacts, my_error = self.get_ops_receivers(hostname)
            emails = [each.get("email") for each in ops_contacts]
            user_ids = [each.get("employee_id") for each in ops_contacts]
            emails = [
                x for x in emails
                if self.employee_info_api.is_employee_email_valid(x)
            ]
            user_ids = [
                x for x in user_ids
                if self.employee_info_api.is_employee_uid_valid(x)
            ]
        return user_ids, emails, my_error

    def payloads_for_ops_director(self, to_wechat, to_email, email_title,
                                  content):
        # for ops director specially
        payloads = []
        if not to_wechat:
            return payloads

        # if send wechat ,send email by the way
        ops_director_email = self.persons_contact.get("ops_director").get(
            "email")

        if ops_director_email not in to_email:
            names = [
                self.employee_info_api.get_employee_contact_by_email(email).
                get("displayName") for email in to_email
            ]

            payload_email = {
                "send_type": "email",
                "to": [ops_director_email],
                "content": " 告警接收人员: {}\n".format(",".join(names)) + content,
                "subject": email_title,
            }

            payloads.append(payload_email)

        names = [
            self.employee_info_api.get_employee_contact_by_uid(uid).get(
                "displayName") for uid in to_wechat
        ]

        payload_wechat = {
            "send_type": "wechat",
            "to":
            [self.persons_contact.get("ops_director").get("employee_id")],
            "content": " 告警接收人员: {}\n".format(",".join(names)) + content,
        }

        payloads.append(payload_wechat)

        return payloads

    def elastalert_payload_2_amc_payload(self, payload):
        match_string = payload["match"].strip()
        alert_name, hostname = self.parse_elastalert_match_str(match_string)

        # from elast alert rule
        user_ids, emails = self.parse_user_ids_and_emails_from_elastalert(
            payload)

        # judge from CMDB
        user_ids, emails, my_error = self.get_receivers_from_CMDB_if_no_receivers_given(
            user_ids, emails, hostname)

        # remove dupricate
        to_email = [x for x in set(emails) if x]
        to_wechat = [x for x in set(user_ids) if x]

        # I will send another to ops director
        to_wechat = [
            x for x in to_wechat
            if x != self.persons_contact.get("ops_director").get("employee_id")
        ]

        # generate payload
        content = my_error + "\n" + match_string

        payloads = []

        subject = payload.get("subject")
        email_title = subject if subject else "ElastAlert: " + alert_name

        if to_email != []:
            payloads.append({
                "send_type": "email",
                "to": to_email,
                "content": content,
                "subject": email_title,
            })
        if to_wechat != []:
            payloads.append({
                "send_type": "wechat",
                "to": to_wechat,
                "content": content,
                # 'to_tag': tag_ids
            })

        payloads += self.payloads_for_ops_director(to_wechat, to_email,
                                                   email_title, content)
        return payloads, alert_name, hostname


class CMDB_API:
    def __init__(self, ip, user, password):
        self.auth = requests.auth.HTTPBasicAuth(user, password)
        self.url_prefix = "{}/api/".format(ip)

    def api(self, query):
        return requests.get(self.url_prefix + query, auth=self.auth)

    def get_node_info_by_hostname(self, hostname):
        return self.api("asset/node/tag/?hostname=" + hostname)


def send_alerts(payloads, alert_name, hostname):
    for payload in payloads:
        # send
        data, status_code = send_alert(payload)
        if status_code != 201:
            if payload["send_type"] == "wechat":
                app.logger.warning("Send wechat Fail ", data)
                payload["content"] = textwrap.dedent("""
                    告警名称: {}
                    告警主机: {}
                    微信发送告警内容失败，告警内容请查看您的邮件，谢谢
                    """.format(alert_name, hostname))
                send_alert(payload)

    # I assume elastalert alerter does not react of this response
    return {
        "data": {},
        "error": {
            "code": 0,
            "message": "this is a correct way"
        }
    }, 201


def send_elastalert_alert(payload):
    app.logger.info("Received ElastAlert Payload: %s",
                    json.dumps(payload, ensure_ascii=False))
    # record_in_test_db('ElastAlert', payload)

    payloads, alert_name, hostname = send_elast_alert.elastalert_payload_2_amc_payload(
        payload)

    return send_alerts(payloads, alert_name, hostname)


def send_kapacitor_alert(payload):
    headers = dict(request.headers.to_wsgi_list())
    app.logger.info("Received KapacitorAlert Payload: %s",
                    json.dumps(payload, ensure_ascii=False))
    app.logger.info("Received KapacitorAlert Headers: %s",
                    json.dumps(headers, ensure_ascii=False))

    # host_name = payload.get('data').get('series').get('tags').get('host')

    # record_in_test_db('KapacitorAlert', payload, headers)

    alert_content = payload.get("details")

    level = payload.get("level")
    previous_level = payload.get("previousLevel")
    severity = "crit" if "CRITICAL" in (level, previous_level) else "warn"

    emails = sorted(
        list(set(request.headers.get(severity + "_email").split("|"))))
    wechats = sorted(
        list(set(request.headers.get(severity + "_wechat").split("|"))))
    wechats = "|".join(wechats)

    payload = {
        "match": alert_content,
        "email": emails,
        "user_id": wechats,
        "subject": payload.get("message"),
    }

    return send_elastalert_alert(payload)


send_elast_alert = SendElastAlert()
