﻿#!/usr/bin/env python
import codecs
import json
import traceback

import os
import re
import smtplib
import subprocess
import time
from email.header import Header
from email.mime.text import MIMEText

from flask import request, jsonify, send_from_directory

import connexion
import markdown2
from elasticsearch import Elasticsearch
from elasticsearch.connection import create_ssl_context
from elasticapm.contrib.flask import ElasticAPM
from .config import config_object

from wechatpy.enterprise import WeChatClient
from wechatpy.exceptions import WeChatClientException
from top.api.base import TopException

import top.api

# pylint: disable=unused-import
from ndf_devops_utils.utils import init_logger, GunicornLogger
from ndf_devops_utils.api.employee_info_api import EmployeeInfoApi

ROOT_PATH = os.path.join(os.path.split(os.path.realpath(__file__))[0], "..")
connex = connexion.FlaskApp(
    __name__, specification_dir=os.path.join(ROOT_PATH, "api/v1/"))
app = connex.app
app.debug = (
    True
)  # gunicorn will only output WARN level log, so if you need debug and info log, set it True
app.config.from_object(config_object)
init_logger(app,
            app.config.get("LOG").get("path"),
            app.config.get("LOG").get("level"))
ElasticAPM(app)
EMPLOYEE_INFO_API = EmployeeInfoApi(
    app.config.get("EMPLOYEE_INFO_URL"),
    app.config.get("EMPLOYEE_INFO_USER"),
    app.config.get("EMPLOYEE_INFO_PASSWORD"),
    app.logger,
)
EMPLOYEE_INFO_API.get_all_users()

ERR_CODE = "code"
ERR_MSG = "message"
TTS = "tts"
EMAIL = "email"
WECHAT = "wechat"
CHANNEL = "channel"
DOCKER = "docker"
ALERT = "alert"
ZABBIX = "zabbix"
SEND_TYPE = "send_type"
TO = "to"
FUNC = "func"
CONTENT = "content"
KEYWORD = "keyword"
BIZ_ID = "biz_id"
ALL = "all"


class SendWechat:
    def __init__(self):
        self.client = {}
        self.agent_name_2_id = {}
        for agent_name in ("alert", "docker", "zabbix"):
            agent_id = app.config.get("WECHAT").get(f"{agent_name}_agent_id")
            self.client[agent_id] = WeChatClient(
                app.config.get("WECHAT").get(f"{agent_name}_id"),
                app.config.get("WECHAT").get(f"{agent_name}_secret"),
            )
            self.agent_name_2_id[agent_name] = agent_id
        self.default_agent_id = app.config.get("WECHAT").get("alert_agent_id")

    def send_message(self, to, content, agent_id=None):
        agent_id = self.default_agent_id if agent_id is None else agent_id
        # in case users input number
        agent_id = str(agent_id)
        self.client[agent_id].message.send_text(agent_id, to, content)

    def channel_2_agent_id(self, channel):
        agent_id = self.agent_name_2_id.get(channel)
        if not agent_id:
            agent_id = self.default_agent_id
        return agent_id

    @staticmethod
    def get_name_by_uid(uid):
        contact = EMPLOYEE_INFO_API.get_employee_contact_by_uid(uid)
        if contact:
            return contact.get("displayName")
        return ""

    def __log_send(self, uids, content, tag_id, channel):
        log_json = {
            "send_type":
            WECHAT,
            "to":
            ["{}({})".format(uid, self.get_name_by_uid(uid)) for uid in uids],
            "content":
            content,
            "tag_id":
            tag_id,
            "channel":
            channel,
        }
        app.logger.info("Send %s: %s", WECHAT, json.dumps(log_json))

    def send_wechat(self, uids, content, channel=None, tag_id=None):
        # handle the sick unicode escape like, u'\\u76d1\\u63a7\\u5e73\\u53f0',
        # which wechat can not send
        content = re.sub(
            r"(\\u[0-9A-Fa-f]+)",
            lambda matchobj: chr(int(matchobj.group(0)[2:], 16)),
            content,
        )
        # wechat :{"errcode":45002,"errmsg":"content size out of limit"} upper limit 2000 bytes
        # 2000/3=666
        content = content[:660] + ("......" if len(content) >= 660 else "")
        # record the send action, no need to keep result, error result will be
        # recorded by another way
        try:
            agent_id = self.channel_2_agent_id(channel)

            self.send_message(uids, content, agent_id)

            self.__log_send(uids, content, tag_id, channel)

        except WeChatClientException as err_str:
            app.logger.debug(traceback.format_exc())
            # I want to record more info in log
            raise ValueError(WECHAT, uids, err_str)


class SendTts:
    """
    reference : https://api.alidayu.com/doc2/apiDetail.htm?spm=a3142.8062978.3.2.1d381f94XgAkoz&apiId=25444
    """

    ILLEGAL_INFO = "非法告警信息"
    # space is not allowed...
    FORBIDDEN_TOKEN = ",.#/: "

    def __init__(self):
        url = app.config.get("TTS").get("url")
        port = app.config.get("TTS").get("port")
        https = app.config.get("TTS").get("https")
        self.req = top.api.AlibabaAliqinFcTtsNumSinglecallRequest(
            url, port, https)
        appkey = app.config.get("TTS").get("appkey")
        secret = app.config.get("TTS").get("secret")
        self.req.set_app_info(top.appinfo(appkey, secret))
        self.called_show_num = app.config.get("TTS").get("caller")
        self.called_show_num_bak = app.config.get("TTS").get("caller_bak")
        self.req.called_show_num = self.called_show_num
        self.req.tts_code = app.config.get("TTS").get("tts_code")

    @staticmethod
    def _filter_forbidden_char(content):

        if content == "":
            return SendTts.ILLEGAL_INFO

        try:
            # content with all num is not allowed. shit
            int(content)
            return SendTts.ILLEGAL_INFO
        except BaseException:
            pass

        for _token in SendTts.FORBIDDEN_TOKEN:
            content = content.replace(_token, "")

        return content

    def switch_channel(self):
        self.req.called_show_num = (
            self.called_show_num_bak
            if self.req.called_show_num == self.called_show_num else
            self.called_show_num)

    def __log_send(self, mobile, content):
        mobile_with_name = "{}({})".format(mobile,
                                           self.get_name_by_mobile(mobile))
        # you must record this first, or else if getResponse raise exception
        # that you have no chance then
        log_json = {
            "send_type": TTS,
            "to": [mobile_with_name],
            "content": content
        }
        app.logger.info("Send %s: %s", TTS,
                        json.dumps(log_json, ensure_ascii=False))

    def __send(self, mobile, content):
        # tts_param is used to fill template message
        # which is "您有一条告警信息${message},请尽快处理"
        self.req.tts_param = '{{"message":{}}}'.format(content)
        self.req.called_num = mobile
        return self.req.getResponse()

    def send_tts(self, mobiles, content):
        """
        after the request, we will need some time, at least 30s to get real result,
        which is collected by another service.
        I return unique biz_id for you, you can check whether this call has been listened or not
        """
        content = self._filter_forbidden_char(content)
        biz_ids = []

        for mobile in mobiles:
            resp = self.__send(mobile, content)
            app.logger.info("Send %s Response: %s", TTS,
                            json.dumps(resp, ensure_ascii=False))
            # !! the key success or err_code does not mean anything, even you give a shit number
            biz_ids.append(
                resp["alibaba_aliqin_fc_tts_num_singlecall_response"]["result"]
                ["model"])

            self.__log_send(mobile, content)

        return biz_ids

    @staticmethod
    def __get_contact_by_mobile(mobile, contact_type):
        contact = EMPLOYEE_INFO_API.get_employee_contact_by_mobile(mobile)
        if contact:
            return contact.get(contact_type)
        return ""

    def get_name_by_mobile(self, mobile):
        return self.__get_contact_by_mobile(mobile, "displayName")

    def get_uid_by_mobile(self, mobile):
        return self.__get_contact_by_mobile(mobile, "sAMAccountName")

    def get_email_by_mobile(self, mobile):
        return self.__get_contact_by_mobile(mobile, "mail")

    def send_alert_with_wechat_email_if_tts_fail(self, to, content):
        new_content = f"你的TTS语音告警发送不成功，我(AMC)尝试通过这个渠道通知你：\n{content}"

        uids = [self.get_uid_by_mobile(mobile) for mobile in to]
        WECHAT_SEND.send_wechat(uids, new_content)
        emails = [self.get_email_by_mobile(mobile) for mobile in to]
        EMAIL_NDF.send_email_with_auto_login(
            emails, new_content, subject="你有一条TTS语音告警因发送不成功，内容见正文")


class GetTtsResultFromES:
    def __init__(self):
        ES_USER = app.config.get("ES").get("user")
        ES_PASSWORD = app.config.get("ES").get("password")
        ES_HOST = app.config.get("ES").get("url")
        ES_PORT = app.config.get("ES").get("port")
        ES_INDEX = app.config.get("ES").get("index")

        pem_path = os.path.join(ROOT_PATH, "cert/niudingfeng.pem")
        ssl_context = create_ssl_context(cafile=pem_path)
        self.es = Elasticsearch(
            hosts=[{
                "host": ES_HOST,
                "port": ES_PORT
            }],
            ssl_context=ssl_context,
            scheme="https",
            http_auth=(ES_USER, ES_PASSWORD),
        )
        self.tts_index_in_es = ES_INDEX
        self.interest_keys = ["message"]

    @staticmethod
    def _es_json2normal_json(es_json):
        return [
            json.loads(each["_source"]["message"])
            for each in es_json["hits"]["hits"] if each["_source"] != {}
        ]

    def find_by_keyword(self, keyword, limit):
        # pylint: disable=unexpected-keyword-arg
        es_json = self.es.search(
            index=self.tts_index_in_es,
            q=keyword,
            _source_include=self.interest_keys,
            size=limit,
        )
        return self._es_json2normal_json(es_json)

    def is_tts_listened(self, bizid):
        # pylint: disable=unexpected-keyword-arg
        es_json = self.es.search(
            index=self.tts_index_in_es,
            q=bizid,
            _source_include=self.interest_keys)

        ret_hits = es_json["hits"]["hits"]

        if len(ret_hits) == 0:
            raise ValueError("There is no such {} in ES".format(BIZ_ID))
        elif len(ret_hits) > 1:
            raise RuntimeError(
                "ES has dupricate bizids, inform someone to handle")

        message = json.loads(ret_hits[0]["_source"]["message"])
        # app.logger.debug(message)

        # biz_id is too long, ES will match the first 12 characters
        # for example:
        # biz_id 113872760168^100691910168 exists
        # biz_id 113872760168^100691910167 not exists
        # if you search the latter one, ES will return the former one.
        # so you have to judge the return biz_id whether is what you request

        if bizid != message[BIZ_ID]:
            raise ValueError("There is no such {} in ES".format(BIZ_ID))

        ret = (True if int(message["status_code"]) == 200_000
               and int(message["duration"] > 4) else False)
        return {"listened": ret}


class SendEmail:
    def __init__(self):
        self.user_ndf_mon = app.config.get("EMAIL").get("user")
        self.psssword_ndf_mon = app.config.get("EMAIL").get("password")
        self.email_ndf_mon = app.config.get("EMAIL").get("email")
        self.smtp_server_ip = app.config.get("EMAIL").get("smtp_server")
        self.smtp_server = smtplib.SMTP()

    def stmp_login(self):
        # self.smtp_server.set_debuglevel(1)
        # default port is 25
        self.smtp_server.connect(self.smtp_server_ip)
        self.smtp_server.ehlo()
        self.smtp_server.login(self.user_ndf_mon, self.psssword_ndf_mon)

    def __log_send(self, receivers, content):
        receivers_with_name = [
            "{}({})".format(email, self.get_name_by_email(email))
            for email in receivers
        ]
        log_json = {
            "send_type": EMAIL,
            "to": receivers_with_name,
            "content": content
        }
        app.logger.info("Send %s: %s", EMAIL,
                        json.dumps(log_json, ensure_ascii=False))

    def send_email(self, receivers, content="", subject=""):
        """
        receivers can be a list [a@email, b@email] or a string 'a@email'
        """
        message = MIMEText(content, "plain", "utf-8")
        message["From"] = self.email_ndf_mon
        message["Subject"] = Header(subject, "utf-8")
        message["To"] = ", ".join(receivers)

        self.smtp_server.sendmail(self.email_ndf_mon, receivers,
                                  message.as_string())

        self.__log_send(receivers, content)

    def send_email_with_auto_login(self, to, content="", subject=""):
        try:
            self.send_email(to, content=content, subject=subject)
        except (
                smtplib.SMTPServerDisconnected,
                smtplib.SMTPSenderRefused,
                smtplib.SMTPAuthenticationError,
        ) as e:
            app.logger.debug(traceback.format_exc())
            app.logger.info(e)
            self.stmp_login()
            self.send_email(to, content=content, subject=subject)

    @staticmethod
    def get_name_by_email(email):
        contact = EMPLOYEE_INFO_API.get_employee_contact_by_email(email)
        if contact:
            return contact.get("displayName")
        return ""


def set_error_response(response, error_message, error_code=-1):
    response["error"][ERR_MSG] = error_message
    response["error"][ERR_CODE] = error_code
    return response


def __send_email(to, content, subject=None):
    subject = "Alert!!" if subject is None else subject
    EMAIL_NDF.send_email_with_auto_login(to, content, subject)


def __send_wechat(to, content, channel, tagid):
    WECHAT_SEND.send_wechat(to, content, channel, tagid)


def __send_tts(to, content, response):
    resend_fail_counter = 0
    while resend_fail_counter < 2:
        if resend_fail_counter > 0 and not resend_fail_counter % 2:
            time.sleep(35)
        try:
            response["data"]["items"] = TTS_ALI.send_tts(to, content)
            break
        except TopException as e:
            app.logger.debug(traceback.format_exc())
            err_str = "{}: {}".format(type(e).__name__, e)
            app.logger.warning("Send %s Failed: %s", TTS, err_str)
            if "TopException" in err_str and "BUSINESS_LIMIT_CONTROL" in err_str:
                TTS_ALI.switch_channel()
                resend_fail_counter += 1
            else:
                content = "有一条告警语音发送不成功，请从你的邮件和微信查阅该告警信息"
                continue
    else:
        err_str = "Tts Resend Failed !!"
        content += "\n（备注：阿里规定，同一个模板或语音文件，同一个被叫。流控规则：2次/分钟，50次/天。不满足流控规则，无法发送，并产生BUSINESS_LIMIT_CONTROL错误）"
        TTS_ALI.send_alert_with_wechat_email_if_tts_fail(to, content)

        # 408 Request Timeout
        raise Exception("send_fail", err_str, 408)


def send_alert(args):
    app.logger.debug("Received POST Payload: %s",
                     json.dumps(args, ensure_ascii=False))
    send_type, to, content = args["send_type"], args["to"], args["content"]

    if not isinstance(to, (list, tuple)):
        to = [to]

    response = {"error": {ERR_CODE: 0, ERR_MSG: ""}, "data": {}}
    response_code = 201

    try:
        if send_type == EMAIL:
            subject = args.get("subject")
            __send_email(to, content, subject)
        elif send_type == WECHAT:
            __send_wechat(to, content, args.get("channel"), args.get("to_tag"))
        elif send_type == TTS:
            __send_tts(to, content, response)
    except Exception as e:  # pylint: disable=broad-except
        app.logger.debug(traceback.format_exc())
        err_str = "{}: {}".format(type(e).__name__, e)
        if "send_fail" in e.args:
            response_code = int(e.args[-1])
            if e.args[-2]:
                err_str = e.args[-2]
        else:
            # 400 Bad Request
            response_code = 400
        if 400 <= response_code < 500:
            app.logger.warning("Send Fail: %s", err_str)
        else:
            app.logger.error("Send Fail: %s", err_str)
        set_error_response(response, err_str)
    # data, status_code
    return response, response_code


def find_by_bizid(biz_id):
    response = {"error": {ERR_CODE: 0, ERR_MSG: ""}, "data": {}}
    response_code = 200

    try:
        response["data"] = ES_CLIENT.is_tts_listened(biz_id)
    except (ValueError, RuntimeError) as e:
        app.logger.debug(traceback.format_exc())
        err_str = "{}: {}".format(type(e).__name__, e)
        app.logger.warning("Find TTS Result From ES FAIL: %s", err_str)
        set_error_response(response, err_str)

    return response, response_code


def find_by_keyword(keyword, limit):
    response = {"error": {ERR_CODE: 0, ERR_MSG: ""}, "data": {}}
    response_code = 200

    try:
        response["data"]["items"] = ES_CLIENT.find_by_keyword(keyword, limit)
    except (ValueError, RuntimeError) as e:
        app.logger.debug(traceback.format_exc())
        err_str = "{}: {}".format(type(e).__name__, e)
        app.logger.warning("Find TTS Result From ES FAIL: %s", err_str)
        set_error_response(response, err_str)

    return response, response_code


def markdown_2_html(markdown_file="README.md",
                    html_file="README.html",
                    encoding="utf-8"):
    content = codecs.open(markdown_file, mode="r", encoding=encoding).read()
    html = markdown2.markdown(content, extras=["fenced-code-blocks"])
    codecs.open(
        html_file, "w", encoding=encoding,
        errors="xmlcharrefreplace").write(html)


@app.route("/")
def readme():
    """
    will display the README.html in static folder
    """
    readme_html = "README.html"
    markdown_2_html()
    os.system("mv {} static".format(readme_html))
    return send_from_directory(os.path.join(ROOT_PATH, "static"), readme_html)


@app.route("/api/v1/debug")
def debug_info():
    app.logger.info("get debug")

    exit_code, ip_all = subprocess.getstatusoutput("ip a")
    exit_code, version = subprocess.getstatusoutput("cat VERSION")

    exit_code, git_hash = subprocess.getstatusoutput("cat HEAD")

    if exit_code:
        exit_code, git_hash = subprocess.getstatusoutput("git rev-parse HEAD")

    return jsonify({"ip": ip_all, "version": version, "git_hash": git_hash})


@app.route("/api/v1/swagger.yaml")
def swagger_yaml():
    return send_from_directory(
        os.path.join(ROOT_PATH, "api", "v1"), "swagger.yaml")


@app.route("/api/v1/swagger.json")
def swagger_json():
    return send_from_directory(
        os.path.join(ROOT_PATH, "api", "v1"), "swagger.json")


@app.route("/v1/alert", methods=["POST"])
@app.route("/send", methods=["POST"])
def send_alert_old_api():
    content, status_code = send_alert(request.get_json())
    return jsonify(content), status_code


@app.route("/v1/alert/tts/findByBizid", methods=["GET"])
def find_by_bizid_old_api():
    content, status_code = find_by_bizid(request.args.get("biz_id"))
    return jsonify(content), status_code


@app.route("/v1/alert/tts/findByKeyword", methods=["GET"])
def find_by_keyword_old_api():
    content, status_code = find_by_keyword(
        request.args.get("keyword"), request.args.get("limit"))
    return jsonify(content), status_code


connex.add_api("swagger.yaml")
# ref: http://elasticsearch-py.readthedocs.io/en/master/
ES_CLIENT = GetTtsResultFromES()
TTS_ALI = SendTts()
EMAIL_NDF = SendEmail()
EMAIL_NDF.stmp_login()
WECHAT_SEND = SendWechat()
